package view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.themealdb.R;

/**
 * webservices am biespel MealDbAPI
 *
 * Api
 * {"meals":
 * [{"idMeal":"52911",
 * "strMeal":"Summer Pistou",
 * "strDrinkAlternate":null,
 * "strCategory":"Vegetarian",
 * "strArea":"French",
 * "strInstructions":"Heat the oil in a large pan and fry the leeks and courgette for 5 mins to soften. Pour in the stock, add three-quarters of the haricot beans with the green beans, half the tomatoes, and simmer for 5-8 mins until the vegetables are tender.\r\nMeanwhile, blitz the remaining beans and tomatoes, the garlic and basil in a food processor (or in a bowl with a stick blender) until smooth, then stir in the Parmesan. Stir the sauce into the soup, cook for 1 min, then ladle half into bowls or pour into a flask for a packed lunch. Chill the remainder. Will keep for a couple of days.",
 * "strMealThumb":"https:\/\/www.themealdb.com\/images\/media\/meals\/rqtxvr1511792990.jpg",
 * "strTags":null,"strYoutube":"https:\/\/www.youtube.com\/watch?v=fyeqZJKEXp0",
 * "strIngredient1":"Rapeseed Oil",
 * "strIngredient2":"Leek",
 * "strIngredient3":"Courgettes",
 * "strSource":"https:\/\/www.bbcgoodfood.com\/recipes\/summer-pistou",
 * "dateModified":null}]}
 */

public class MainActivity extends AppCompatActivity {
    private TextView textView;
    private void updateView(String text){
        textView.append(text);
        textView.append("\n");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        updateView("hello");
        updateView("world");

    }
}